Milo Project
============

The Milo Project is an initiative to allow teachers to easily create and share
digital educational ressources, using the excellent H5P framework. The 
inspiration comes from sites like JSFiddle, which provide a great playing 
ground for people to explore new ideas, but also a very easy way to share
and build solutions.

Drupal provides an excellent starting point for any such projects. This 
distriubution is heavily oriented towards a certain goal, and has a strict
philosophy regarding UX and UI. As such, it may not be suited as a starting
point for building very different solutions, but should rather be used as-is.

