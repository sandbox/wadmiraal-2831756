<?php

/**
 * Implements theme_preprocess_page().
 */
function milo_theme_preprocess_page(&$vars) {
  if (!isset($vars['user_menu'])) {
    if ($user_menu_tree = menu_tree_all_data('user-menu')) {
      $vars['user_menu'] = menu_tree_output($user_menu_tree);
    }
  }
}

/**
 * Implements hook_theme().
 */
function milo_theme_theme() {
  return array(
    'h5p_content_node_form' => array(
      'render element' => 'form',
      'template' => 'node-form--h5p_content',
      'path' => drupal_get_path('theme', 'milo_theme'),
    ),
  );
}

/**
 * Implements theme_preprocess_h5p_content_node_form()
 */
function milo_theme_preprocess_h5p_content_node_form(&$vars) {
  $vars['form']['title']['#title'] = $vars['form']['title']['#attributes']['placeholder'] = t("Enter a name");

  $vars['form']['h5p_editor']['#title'] = t("Choose a type");

  $vars['form']['h5p_type']['#default_value'] = 'create';
}

