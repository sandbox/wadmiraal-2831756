<div class="node-form--h5p-content">
  <div class="node-form__title">
    <?php print render($form['title']); ?>
  </div>

  <div class="node-form__h5p-editor">
    <?php print render($form['h5p_editor']); ?>
  </div>

  <div class="node-form__hidden">
    <?php print render($form['h5p_type']); ?>
    <?php print render($form['h5p']); ?>
  </div>

  <?php print drupal_render_children($form); ?>   
</div>
